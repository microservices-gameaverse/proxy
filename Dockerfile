#Import base container as Nginx running on Alpine Linux
FROM nginx:alpine

#Remove base configuration for nginx
RUN rm /etc/nginx/conf.d/*

#Copy files needed to run the application
COPY proxy.conf /etc/nginx/conf.d/
COPY ./ssl /etc/nginx/certs/
